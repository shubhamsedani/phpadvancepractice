<?php
  function mergesort(&$Array, $left, $right) {
    if ($left < $right) { 
      $mid = $left + (int)(($right - $left)/2);
      mergesort($Array, $left, $mid);
      mergesort($Array, $mid+1, $right);
      merge($Array, $left, $mid, $right);
    }
  }

  function merge(&$Array, $left, $mid, $right) {
    $n1 = $mid - $left + 1; 
    $n2 = $right - $mid;
    $LeftArray = array_fill(0, $n1, 0); 
    $RightArray = array_fill(0, $n2, 0);
    for($i=0; $i < $n1; $i++) { 
      $LeftArray[$i] = $Array[$left + $i];
    }
    for($i=0; $i < $n2; $i++) { 
      $RightArray[$i] = $Array[$mid + $i + 1];
    }

    $x=0; $y=0; $z=$left;
    while($x < $n1 && $y < $n2) {
      if($LeftArray[$x] < $RightArray[$y]) { 
        $Array[$z] = $LeftArray[$x]; 
        $x++; 
      }
      else { 
        $Array[$z] = $RightArray[$y];  
        $y++; 
      }
      $z++;
    }

    //LeftArray array na elements ne copy krse
    while($x < $n1) { 
      $Array[$z] = $LeftArray[$x];  
      $x++;  
      $z++;
    }

    //RightArray array na elements ne copy krse
    while($y < $n2) { 
      $Array[$z] = $RightArray[$y]; 
      $y++;  
      $z++; 
    }
  }

  // main output will be print here(output array)
  function PrintArray($Array, $n) { 
    for ($i = 0; $i < $n; $i++) 
      echo $Array[$i]." "; 
  } 

  $MyArray = array(3, 0, 2, 5, -1, 4, 1);
  $n = sizeof($MyArray); 
  echo "Original Array";
  PrintArray($MyArray, $n);
  echo "<br>";

  mergesort($MyArray, 0, $n-1);
  echo "Sorted Array";
  PrintArray($MyArray, $n);
?>
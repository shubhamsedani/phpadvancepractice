<?php
    function insertion_Sort($my_array)
    {
        for($i=0;$i<count($my_array);$i++){
            $val = $my_array[$i];  
            $j = $i-1;//INITIALLY IT WILL TAKE THE VERY FIRST ELEMENT IN TO THE SORTED ARRAY WHICH IS J
            while($j>=0 && $my_array[$j] > $val){// IIF MY VERY FIRST ELEMENT IS SMALLER THAN RIGHT ELEMENT THAN ENTER THE LOOP
                $my_array[$j+1] = $my_array[$j];//ASSIGN THE VALUE TO THE FIRST ELEMENT 
                $j--;
            }
            $my_array[$j+1] = $val;
        }
    return $my_array;
    }
    $test_array = array(3, 0, 2, 5, -1, 4, 1);
    echo "Original Array :" .  implode(', ',$test_array ) . "<br>";
    $my_array = (insertion_Sort($test_array));
    echo "Sorted Array :" . implode(', ',$my_array );
?>

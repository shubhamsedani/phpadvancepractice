<?php
session_start();
$username = $pwd = $email = $phone = "";
$emailErr = $passErr =  $nameErr = $phnErr = $imageErr = $hey = $_SESSION["authPass"] = $_SESSION["authEmail"] = "";

if($_POST){
  if(isset($_POST['submit'])){
    $username = test_input($_POST['username']);
    $pwd = test_input($_POST['pwd']);
    $email = test_input($_POST['email']);
    $phone = test_input($_POST['phone']); 

    if (empty($_POST["username"]) || (!preg_match("/^[a-zA-Z-' ]*$/", $_POST['username']))) {
      $nameErr = "Add user name in proper manner";
    }
    
    if (empty($_POST["pwd"])) {
      $passErr = "Password is required";
    } 
    
    if (empty($_POST["email"])) {
      $emailErr = "Email is required";
    } 
    
    if (empty($_POST["phone"]) || (!preg_match("/^[0-9]{10}$/",$_POST['phone']))) {
      $phnErr  = "Add proper phone number";
    } 

  $target_dir = "uploads/";
  $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
  $uploadOk = 1;
  $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

  // Check if image file is a actual image or fake image
    if(empty($name)){
      $imageErr = "Image is required";
    }
    else{
      $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
      if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
      } else {
        echo "File is not an image.";
        $uploadOk = 0;
      }
  }

  if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
    } else {
      if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        $imageErr = "";
        echo "The file ". htmlspecialchars( basename( $_FILES["fileToUpload"]["name"])). " has been uploaded.";
      } 
    }

    if($emailErr == "" && $passErr == "" && $nameErr == "" && $phnErr == "" && $imageErr == ""){
      $myfile = fopen("newfile.txt", "a") or die("Unable to open file!");
      fwrite($myfile, PHP_EOL.json_encode($_POST));
    }

  }
}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
?>
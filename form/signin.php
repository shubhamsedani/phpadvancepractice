<?php
include('php/sign-in.php')
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>PHP Allrounder</title>
    <link rel="stylesheet" href="./css/style.css">
</head> 
<body>
    <div class="form">
        <div class="wrapper">
            <form method="POST" action="" enctype="multipart/form-data">

                <label for="email">email </label>
                <?php echo $emailErr?>
                <input type="email" name="email" id="email" placeholder="Enter a valid email">
                
                <label for="pwd">password </label>
                <?php echo $passErr?>
                <input type="password" name="pwd" id='pwd'placeholder="Enter password" >
            
                <input type='submit' name="submit" value='log-in'>
                <?php echo $signinErr?>
            </form>
        </div>
    </div>
</body>
</html>


<?php
include('php/signup.php')
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>PHP Allrounder</title>
    <link rel="stylesheet" href="./css/style.css">
</head> 
<body>
    <div class="form">
        <div class="wrapper">
            <form method="POST" action="" enctype="multipart/form-data">

                <label for="user">username </label>
                <?php echo $nameErr?>
                <input type='text' name= username id ="user" placeholder="Enter your name" value = "<?= empty($nameErr) ? $username : ""?>">
                
                <label for="pwd">password </label>
                <?php echo $passErr?>
                <input type="password" name="pwd" id='pwd'placeholder="Enter password" value = "<?= empty($passErr) ?  $pwd : "" ?>">
                
                <label for="email">email </label>
                <?php echo $emailErr?>
                <input type="email" name="email" id="email" placeholder="Enter a valid email" value = "<?= empty($emailErr) ? $email : "" ?>">
                
                <label for="phone">phone </label>
                <?php echo $phnErr?>
                <input type="text" name="phone" id="phone" placeholder="Enter your mobile number" value = "<?= empty($phnErr) ? $phone : "" ?>">
                
                <label for="fileToUpload">Select image to upload: </label>
                <?php echo $imageErr?>
                <input type="file" name="fileToUpload" id="fileToUpload">

                <input type='submit' name="submit" value='register'>
                
                <label for="signin">Already have an account? Sign in here. </label>
                <a href="signin.php"><input type='button' name="button" id="signin" value='Sign in'></input></a>
            </form>
        </div>
    </div>
</body>
</html>


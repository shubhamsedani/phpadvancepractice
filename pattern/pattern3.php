<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pattern 3</title>
</head>
<body>
    <form action="#" method="post">
        <input type="number" name="userinput" id="userinput">
        <button type="submit">Submit</button>
    </form>
<?php
    if($_POST){
        $n = $_POST['userinput'];
        for ($i=0; $i < $n ; $i++) { 
            if(($n - $i)>=10){
                for($k = 0;$k<$i;$k++){
                    echo "&nbsp;&nbsp;";
                }
            }else{
                echo str_repeat("&nbsp;&nbsp;",$n-9);
            }
            for($j = -$n ; $j<= $n; $j++){
                if($j==0){
                    continue;
                }
                elseif(abs($j)<=$i){
                    echo("*"); 
                }
                 else{
                    echo ($n-abs($j)+1);
                }
            }
            echo "<br>";
        }
        
    }
?>
</body>
</html>